Usage: taille [-h] [-a] [-d dir] [-n display-size]

    -h : displays usage and exits.

    -a : looks at all files and directories, even with leading dot.

    -d : searches in given dir.
    
    -n : displays the n largest files/dirs. Default 10.
    